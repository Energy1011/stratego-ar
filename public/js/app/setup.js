 // GUI elements
  var elementBtnCreateGame, elementBtnJoinGame, elementBtnReadyGame, elementPlayerName;
  var divReadyGame;

  // Setup board
  var lastTileSelected, lastPieceSelected;
  var pieceCounters;
  var pieceCountersOriginal;
  var interval;
  // SetupTable Array: index = tile number
  var setupTable= [];

function showPieceInSetupTable(tile,pid){
  $('[data-tile="'+tile+'"]').html('<img class="piece-setup-mini" style="" src="/images/p'+pid+'.png">');
}

function toggleShowSelectPiece(pid){
  pieceCounters[pid] <= 0 ? $('[data-pid="'+pid+'"]').hide() : $('[data-pid="'+pid+'"]').show();
}

function checkForBusyTile(){
  if(typeof setupTable[lastTileSelected] == 'undefined') return false; 
  var pid = setupTable[lastTileSelected];
  pieceCounters[pid]++;
  toggleShowSelectPiece(pid);
}

function checkForRemove(){
  if(typeof setupTable[lastTileSelected] == 'undefined') return;
  $('[data-tile="'+lastTileSelected+'"]').html('').html(lastTileSelected);
  checkForBusyTile();
  // add element
  delete setupTable[lastTileSelected];
  lastTileSelected = lastPieceSelected = undefined;
}

function assignPiece(tile, pid){
  setupTable[tile] = pid;
  pieceCounters[pid]--;
  showPieceInSetupTable(tile, pid);
  toggleShowSelectPiece(pid);
}

function checkForAssignPiece(){
  if(typeof lastTileSelected == 'undefined' || typeof lastPieceSelected == 'undefined') return;
  var pid = lastPieceSelected; // piece ID
  if(typeof setupTable[lastTileSelected] != 'undefined' && (pid == setupTable[lastTileSelected])) return;
  checkForBusyTile();
  if(pieceCounters[pid] <=0) return;
  assignPiece(lastTileSelected, pid);
  lastTileSelected = lastPieceSelected = undefined;
}

function checkForReady(){
  var c = 0;
  for(var e in setupTable){
    if(typeof setupTable[e] != 'undefined') c++;
  }
  c == 40 ? elementDivReadyGame.show(): elementDivReadyGame.hide();
}

// Listener to use keypad to assign pieces
document.addEventListener("keydown", keyDownDocument, false);
function keyDownDocument(e) {
  var keyC = e.code;
   var match = keyC.match('Numpad');
  if(typeof match == 'object' && match != null){
    lastPieceSelected = parseInt(keyC.match('Numpad').input.replace('Numpad',''));
  }
}

function createListeners(){
  // -------- LOCAL LISTENERS
  // Create game
  elementBtnCreateGame.on('click',()=>{
    socket.emit('client:create-game',{
      plName:elementPlayerName.val()
    });
  });

  //INFO: 1 flag, 1 special , 6 bombs, 32 soldiers, 
  //Ready to play
  elementBtnReadyGame.on('click',()=>{
    socket.emit('client:ready-game',{
      gID:gD.gID,
      plID:gD.plID,
      plName:elementPlayerName.val(),
      plT: setupTable
    });
    $(elementBtnReadyGame).hide();
  });

  // Setup board
  $('.tile-setup').click(function(){
    lastTileSelected = parseInt($(this).attr('data-tile'));
    checkForAssignPiece();
    if(typeof lastPieceSelected == 'undefined') checkForRemove();
    checkForReady();
  });

  $('.piece-setup').click(function(){
    lastPieceSelected = parseInt($(this).attr('data-pid'));
  });

}

function guiShowTableSetup(){
  $('#table-setup-div').show();
  $('#join-form-game-div').hide();
}

function setGameData(res){
  gD = res;
  pieceCountersOriginal = res.pieceCounters.slice();
  pieceCounters = res.pieceCounters.slice();
}

function checkOpponentReadyGame(){
  socket.emit('client:check-opponent-ready-game',{
    gID: gD.gID
  });
}

// -------- SERVER EVENTS
socket.on('server:create-game',function(res){
  console.log('server:create-game');
  $('#icons-share').html('<a href="'+BASEDIR+'setup/'+res.gID+'" target="_blank">Share Game Link</a><br>');
  $('#icons-share').append('<a href="//telegram.me/share/url?url='+BASEDIR+'setup/'+res.gID+'&amp;text=Play in Stratego Versus Me !" class=""><img src="http://icons.iconarchive.com/icons/froyoshark/enkel/512/Telegram-icon.png" style="width:40px;"></a>');
  $('#icons-share').append('<a href="whatsapp://send?text='+BASEDIR+'setup/'+res.gID+'&amp;text=Play in Stratego Versus Me !" class=""><img src="https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Ficons.iconarchive.com%2Ficons%2Fdtafalonso%2Fandroid-l%2F512%2FWhatsApp-icon.png&f=1" style="width:40px;"></a>');
  setGameData(res);
  guiShowTableSetup();
});

socket.on('server:join-game',(res)=>{
  console.log('server:join-game');
  if(typeof res.error != 'undefined'){
    alert(res.error);
    window.location = BASEDIR;
  }else{
    setGameData(res);
    guiShowTableSetup();
  }
});

socket.on('server:ready-game',(res)=>{
  $('.gui-info').html('Waiting for a player....');
  console.log('server:ready-game');
  if(typeof res.error == 'undefined'){
    interval = setInterval(checkOpponentReadyGame, 5000);
  }else{
    $('.gui-info').html(res.error);
  }
});

socket.on('server:check-opponent-ready-game',(res)=>{
  console.log('server:check-opponent-ready-game');
  if(res.status == GAMESTATES.READY){
    $('.gui-info').html('Opponent ready !');
    setTimeout(()=>{
      clearInterval(interval);
      var url = BASEDIR+'game/'+gD.gID+'/'+gD.plID;
      window.location = url;
    }, 3000);
  }
});

function emitJoinGame(gID){
  socket.emit('client:join-game',{gID: gID});
}

function checkJoinToExistingGame(){
  var fields = window.location.pathname.split('/');
  var gID = fields[2];
  if(gID == 0){
    $(elementBtnCreateGame).show();
    return;
  } 
  gD.gID = gID;
  $(elementBtnCreateGame).hide();
  $(elementBtnJoinGame).show();
  $(elementBtnJoinGame).click(function(){emitJoinGame(gID);});
  //guiShowTableSetup();
}

// TODO: remove this DEV
$('#elementBtnDev').click(function(){dev();});
function dev(){
  // add table piece
  setupTable = [];
  pieceCounters = pieceCountersOriginal.slice();
  var tile = 0;
  for(let pid in pieceCounters){
   for(let e = 0 ; pieceCounters[pid] > 0; e++){
     assignPiece(tile, pid);
     tile++;
   }
  }
  checkForReady();
}

$(document).ready(function(){
  // GUI elements
  elementBtnJoinGame = $('#elementBtnJoinGame');
  elementBtnReadyGame = $('#elementBtnReadyGame');
  elementBtnCreateGame= $('#elementBtnCreateGame');
  elementPlayerName= $('#elementPlayerName');
  elementDivReadyGame= $('#ready-game-div');

  createListeners();
  checkJoinToExistingGame();
});
