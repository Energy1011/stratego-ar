var selector = undefined;

function completePlTable(){
  for(e in posis){
    if(e < 40) continue;
    if(e >= 40 & e <= 59) gD.plTable[e] = TILETYPE.EMPTY;
    if(e > 59) gD.plTable[e] = TILETYPE.ENEMY;
  }
}

function initGameData(){
  // join room
  socket.emit('client:game-player-join-room',{gID: gD.gID});
  completePlTable();
  //gD.state = GAMESTATES.READY;
}

function getGameDataFromServer(){
  // global game data is passed by render data
  if(typeof gD.error != 'undefined'){
    alert(gD.error);
    document.location = BASEDIR;
  }
  initGameData();
  return true;
}

document.addEventListener("DOMContentLoaded", function(event) {
  // Get gameData from server
  // Init scene
  var sceneEl = document.querySelector('a-scene');
  var markerEl = document.querySelector('#markerEl');

  // scene is ready ?
  if (getGameDataFromServer() && sceneEl.hasLoaded ) {
    run();
  } else {
    sceneEl.addEventListener('loaded', run);
  }

  function run () {
    var lastSelectOrigin = undefined;
    var lastSelectTarget = undefined;

    // info ui 
    var uiInfoText = document.querySelector('#ui-info-text');
    var uiInfoDownText = document.querySelector('#ui-info-down-text');
    var uiInfoImages = document.querySelector('#ui-info-images');
    var uiLastImageSelected = 0;
    var uiBattleInfo = document.querySelector('#ui-battle-info');
    var uiBattleOImg= document.querySelector('#ui-battle-o-img');
    var uiBattleTImg = document.querySelector('#ui-battle-t-img');
    var uiBattleResTxt= document.querySelector('#ui-battle-res-txt');

    // create table
    var table = document.createElement('a-gltf-model');
    table.setAttribute('src','/models/table.gltf');
    table.setAttribute('scale', {x: 0.1, y: 0.1, z: 0.1});
    markerEl.appendChild(table);

    // add pieces 2d images to the info ui area and ui info battle results
    for(var e = 0; e < 13; e++){

      // battle results info
      var img = document.createElement('img');
      img.setAttribute('src','/images/p'+e+'.png');
      img.setAttribute('id','br-o-img'+e);
      img.setAttribute('class','ui-battle-img');
      img.setAttribute('style','display:none');
      uiBattleOImg.appendChild(img);

      var img1 = document.createElement('img');
      img1.setAttribute('src','/images/p'+e+'.png');
      img1.setAttribute('id','br-t-img'+e);;
      img1.setAttribute('class','ui-battle-img');
      img1.setAttribute('style','display:none');
      uiBattleTImg.appendChild(img1);

      // ui info selection area
      var img2 = document.createElement('img');
      img2.setAttribute('src','/images/p'+e+'.png');
      img2.setAttribute('id','img-'+e);
      img2.setAttribute('class','ui-info-img');
      uiInfoImages.appendChild(img2);
    }

    // create pieces
    var pieces = [];
    for(e in posis){
      // Check for empty tiles
      if(e >= 40 & e <= 59)continue;
      var piece = document.createElement('a-gltf-model');
      piece.setAttribute('src','/models/demo-piece.gltf');
      // piece.setAttribute('cursor-listener','');
      piece.setAttribute('position',posis[e][0]+' '+posis[e][1]+' '+posis[e][2]);
      piece.setAttribute('scale', {x: 0.1, y: 0.1, z: 0.1});
      piece.setAttribute('piece', gD.plTable[e]);
      piece.setAttribute('tile', e);
      //TODO: check for it
      piece.setAttribute('ptype', 'piece');
      piece.addEventListener("mousedown", function(){
        var pnum = this.getAttribute('piece');
        selector.setAttribute('position',posis[pnum][0]+' .3 '+posis[pnum][2]);
      }, false);
      markerEl.appendChild(piece);
    }

    // create main selector
    selector = document.createElement('a-box');
    selector.setAttribute('position','-1.35 .3 1.35');
    selector.setAttribute('scale', {x: 0.25, y: 0.4, z: 0.25});
    selector.setAttribute('material', {opacity: 0.5, color:'yellow'});
    selector.setAttribute('tpos',0);
    markerEl.appendChild(selector);

    function uiSetInfoText(d){
      if(typeof d == 'object'){
        if(typeof d.msg != 'undefined' ) uiInfoText.innerHTML = d.msg;
        if(typeof d.error != 'undefined' ) uiInfoText.innerHTML = d.error;
      }else{
        uiInfoText.innerHTML = d;
      }
    }

    function uiSetInfoDownText(d){
      if(typeof d == 'object'){
        if(typeof d.msg != 'undefined' ) uiInfoDownText.innerHTML = d.msg;
        if(typeof d.error != 'undefined' ) uiInfoDownText.innerHTML = d.error;
      }else{
        uiInfoDownText.innerHTML = d;
      }
    }

    function checkPlayerTurn(){
      if(gD.plIndex == gD.lastTurn){
        gD.state = GAMESTATES.PLAYING;
        uiSetInfoDownText('This is your turn');
        return true;
      }else{
        gD.state = GAMESTATES.WAITTURN;
        uiSetInfoDownText('Waiting.. opponent turn');
        return false;
      }
    }

    function checkMoveOneTile(tpos){
      // Just move one tile when player has first selection
      if(typeof lastSelectOrigin !='undefined'){
        // offset distance btwn pieces in this scale
        var op = posis[lastSelectOrigin];
        var offset = .30;
        // target in X axis
        var tx = posis[tpos][0];
        // target in Y axis
        var tz = posis[tpos][2];
        // Check for positions
        if( ((tx < (op[0] - offset).toFixed(2)) || (tx > (op[0] + offset).toFixed(2))) || ((tz < (op[2] - offset).toFixed(2)) || (tz > (op[2] + offset).toFixed(2))) ){
          uiSetInfoText("You can only move your piece one tile of distance");
          return false;
        }
      }
      return true;
    }

    // show image selected in ui info
    function uiShowSelectedPiece(tpos){
      // hide last
      document.querySelector('#img-'+uiLastImageSelected).setAttribute('style','display:none');
      var piece = document.querySelector('[tile="'+tpos+'"]')
      if(piece != null){
        piece = piece.getAttribute('piece');
        if(piece == TILETYPE.ENEMY){
          piece = 12;//enemy ?
          uiSetInfoText('This is an unknown enemy');
        }
        // show current
        uiInfoImages.querySelector('#img-'+piece).setAttribute('style','display:inline');
        uiLastImageSelected = piece;
      }
    }

    function moveSelector(){
      uiSetInfoText('');
      // move in tile units
      var move = this.getAttribute('move');
      // get table position
      var tpos = selector.getAttribute('tpos');
      tpos = parseInt(tpos) - parseInt(move);
      if(tpos > 99 || tpos < 0) return;
      // Check if position exists
      if(typeof posis[tpos] != 'undefined'){
        if(!checkMoveOneTile(tpos)) return;
        selector.setAttribute('position',posis[tpos][0]+' .3 '+posis[tpos][2]);
        selector.setAttribute('tpos',tpos);
        uiShowSelectedPiece(tpos);
      }
    }

    // Set arrow functions
    var arrow = document.getElementById('ui-arrow-up');
    arrow.addEventListener('click',moveSelector,false);
    arrow = document.getElementById('ui-arrow-down');
    arrow.addEventListener('click',moveSelector,false);
    arrow = document.getElementById('ui-arrow-left');
    arrow.addEventListener('click',moveSelector,false);
    arrow = document.getElementById('ui-arrow-right');
    arrow.addEventListener('click',moveSelector,false);

    function checkSelector(){
      if(typeof lastSelectOrigin == 'undefined'){
        // get selector pos
        lastSelectOrigin = parseInt(selector.getAttribute('tpos'));
        // check if there is a piece to move
        if(gD.plTable[lastSelectOrigin] == TILETYPE.EMPTY){
          uiSetInfoText("Here is not a piece to select...");
          return GAMESTATES.ERROR;
        }
        // we have a piece to move ?
        if(gD.plTable[lastSelectOrigin] == 0 || gD.plTable[lastSelectOrigin] == 1){
          uiSetInfoText("Flag and bombs can't move.");
          return GAMESTATES.ERROR;
        }
        // are you trying to move enemy piece ?
        if(gD.plTable[lastSelectOrigin] == TILETYPE.ENEMY){
          uiSetInfoText('Cant move this piece, this is piece is not yours.');
          return GAMESTATES.ERROR;
        }
      }else{
        // get selector pos to target
        lastSelectTarget = parseInt(selector.getAttribute('tpos'));
      }
      if(typeof lastSelectOrigin == 'undefined' || typeof lastSelectTarget == 'undefined') return -1;
      return GAMESTATES.OK;
    }

    function resetLastSelects(){
      lastSelectOrigin = undefined;
      lastSelectTarget = undefined;
    }

    function uiFadeOut(element, o, t) {
      var op = 1;  // initial opacity
      var timer = setInterval(function () {
        if (op <= 0.1){
          clearInterval(timer);
          element.style.display = 'none';
          o.style.display = 'none';
          t.style.display = 'none';
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op -= op * 0.1;
      }, 200);
    }

    function showBattleInfo(d){
      var originImg = document.querySelector('#br-o-img'+d.pieceOVal);
      var targetImg = document.querySelector('#br-t-img'+d.pieceTVal);

      uiBattleInfo.style.display = 'inline-block';
      originImg.style.display = 'inline';
      targetImg.style.display = 'inline';
      uiBattleResTxt.innerHTML = d.msg;
      uiFadeOut(uiBattleInfo, originImg, targetImg);

    }

    function getBattleResultMsg(d){
      var msg = '';
      if(typeof d.winGame != 'undefined'){
        if(d.opponentTurn){
          msg = 'LOSE';
        }else{
          msg = 'WIN';
        }
        d.msg = 'YOU '+msg+' THE GAME';
        return d;
      }

      if(d.bResult == BATTLERESULT.WIN){
        msg = d.opponentTurn ?'LOSE':'WIN';
      }
      if(d.bResult == BATTLERESULT.LOSE){
        msg = d.opponentTurn ?'WIN':'LOSE';
      }
      if(d.bResult == BATTLERESULT.DRAW){
        msg = 'DRAW';
      }
      d.msg = 'YOU '+msg+' THIS BATTLE';
      return d;
    }

    function finishGame(d){
      //TODO: finish game in server
      if(d.opponentTurn){
        alert('YOU LOSE THE GAME');
        window.location = BASEDIR;
      }else{
        alert('YOU WIN THE GAME');
        window.location = BASEDIR;
      }
    }

    function execTurn(d){
      var origin = d.move[0];
      var target = d.move[1];
      var pieceOriginEl = get3dElementByTile(origin);
      var pieceTargetEl = get3dElementByTile(target);

      if(d.turnType == TURNTYPE.SIMPLE){
        // Steps: 1-swap gD.plTable posis, 
        gD.plTable[target] = gD.plTable[origin];
        gD.plTable[origin] = TILETYPE.EMPTY; // now empty last gD.plTable position
        // 2-update 3d model position, 
        pieceOriginEl.setAttribute('position',posis[target][0]+' '+posis[target][1]+' '+posis[target][2]);
        // 3-update tile element
        pieceOriginEl.setAttribute('tile',target);
      }

      if(d.turnType == TURNTYPE.ATTACK){
        // Target tile is removed from server table game too
        if(d.bResult == BATTLERESULT.DRAW){
          gD.plTable[target] = TILETYPE.EMPTY;
          pieceTargetEl.parentNode.removeChild(pieceTargetEl);
        }
        if(d.bResult == BATTLERESULT.WIN){
          gD.plTable[target] = gD.plTable[origin];
          pieceTargetEl.setAttribute('piece', gD.plTable[target]);
        }
        // show battle info
        showBattleInfo(getBattleResultMsg(d));
        // all cases (draw, lose, win) origin tile is set to empty
        gD.plTable[origin] = TILETYPE.EMPTY;
        pieceOriginEl.parentNode.removeChild(pieceOriginEl);
      }
      uiSetInfoText(d);
      uiShowSelectedPiece(selector.getAttribute('tpos'));
      resetLastSelects();
      if(typeof d.winGame != 'undefined') finishGame(d);
    }

    function emitGamePlayerMove(d){
      d.gID = gD.gID;
      d.plID = gD.plID;
      uiSetInfoDownText('Waiting opponent turn...');
      socket.emit('client:game-player-move',d);
    }

    function get3dElementByTile(tile){
      return document.querySelector('[tile="'+tile+'"]');
    }

    // -------- SERVER EVENTS
    socket.on('server:game-player-move',(r)=>{
      if(r.status == GAMESTATES.OK){
        gD.state = GAMESTATES.WAITTURN;
        r.opponentTurn = false;
        execTurn(r);
      }
      if(typeof r.error !='undefined'){
        uiSetInfoText(r.error);
      }
    });

    socket.on('server:game-opponent-turn',(r)=>{
      r.opponentTurn = true;
      execTurn(r);
      gD.lastTurn = r.lastTurn;
      checkPlayerTurn();
    });

    socket.on('server:game-player-join-game',(r)=>{
      //TODO: init only when both player are in
      console.log('game-player-join-game');
      console.log(r);
    });

    // END ----- SERVER EVENTS

    function action(){
      var res = checkSelector();
      if(res != GAMESTATES.OK) return res;
      if(gD.plTable[lastSelectTarget] != TILETYPE.EMPTY && gD.plTable[lastSelectTarget] != TILETYPE.ENEMY){
        uiSetInfoText("You can't move there, you own that position.");
        return GAMESTATES.RESET;// reset
      }

      // Get possible pieces by tile
      var pieceOriginEl = get3dElementByTile(lastSelectOrigin);
      var pieceTargetEl = get3dElementByTile(lastSelectTarget);

      //var pieceTargetEl = document.querySelector('[tile="'+lastSelectTarget+'"]');
      // Is there a empty tile?
      if(gD.plTable[lastSelectTarget] == TILETYPE.EMPTY){
        uiSetInfoText('This is a move.');
        // Simple move to an empty tile
        emitGamePlayerMove({move:[lastSelectOrigin,lastSelectTarget]});
        return GAMESTATES.WAITRESPONSE; // wait for server response
      }

      // Is there an enemy ?
      if([gD.plTable[lastSelectTarget] == TILETYPE.ENEMY]){
        // Here we have an attack
        uiSetInfoText('Battle begins!');
        emitGamePlayerMove({move:[lastSelectOrigin,lastSelectTarget]});
        return GAMESTATES.WAITRESPONSE; // wait for server response
      }
    } 

    // player btn action
    var btnAction = document.getElementById('ui-btn-action');
    btnAction.addEventListener('click',function(){
      if(gD.state == GAMESTATES.WAITTURN){
        uiSetInfoText('Please wait for your turn...');
        uiSetInfoDownText('Waiting opponent\'s turn...');
        return;
      }
      if(gD.state == GAMESTATES.WAITRESPONSE){
        uiSetInfoText('We still waiting for server...');
        return;
      }

      var res = action();

      if(res == GAMESTATES.RESET || res == GAMESTATES.ERROR){
        resetLastSelects();
      }
      if(res == GAMESTATES.CONTINUE){
        // wait for second action/selection
        uiSetInfoText('Move selector again and select an empty tile or an enemy.');
      }
      if(res == GAMESTATES.WAITRESPONSE){
        // wait for server response
        uiSetInfoText('Waiting response from server...');
        gD.state = GAMESTATES.WAITRESPONSE;
      }
    });

    function initTurn(){
      checkPlayerTurn();
    }

    // INIT TURN
    initTurn();

  }// end run function
});
