# Stratego-ar
This a stratego based game, with augmented reality, programmed with ar.js, nodejs, express, sockets.io

## Collaborate
Contact to me if you want to colaborate in ideas, programming, art, 3d modeling, whatever.

## About images
This repo currently does not contain or distribute any image about 'attack on titan'  and it never will have a no lisenced image. There is a video just showing the functionality with some attack on titan's images, soon this game will have its own Creative Common images.

## Install
run server with:
```bash
$npm start
```
Go to your browser at http://localhost:8080/

## Internet play
You can use ngrok to test and play via internet.
1- run ngrok 
2- edit globals js file and change the basedir.