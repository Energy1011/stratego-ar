'use strict'; 

var mod = {};

mod.PIECEBEHAVES = { 'PASSIVE':1,'NORMAL':2, 'KILLFIRST':3, 'BOMBDEACTIVATOR':4};
mod.PIECEMAXPOWER = 10;
mod.pieceCounters = [1,6,8,5,4,4,4,3,2,1,1,1];
// Pieces id by array position
mod.PIECETYPES = {'FLAG':0, 'BOMB':1, 'BOMBDEACTIVATOR': 3, 'SPECIAL':11};
mod.pieceTypes = [
    {
     name:'Flag',
     desc: "This is your flag, hide it from your enemy!",
      behaviors: [mod.PIECEBEHAVES.PASSIVE],
      power: 0,
      move: false,
      totalUnits: 1
    },
    {
     name:'Bomb',
     desc: "This a dangeours Bomb!",
      behaviors: [mod.PIECEBEHAVES.NORMAL],
      power: 0,
      move: false,
      totalUnits: 6
    },
    {
     name:'Soldier',
     desc: "A brave soldier!",
      behaviors: [],
      power: 0,
      move: true,
      powerVariations:[
        0, // Pices of power 0
        0, // Pieces of power 1
        8, // Pieces of power 2
        5, // 5 bombDeactivator with special behavior
        4, // Pieces of power 4
        4, // Pieces of power 5
        4, // Pieces of power 6
        3, // Pieces of power 7
        2, // Pieces of power 8
        1, // Pieces of power 9
        1, // Pieces of power 10
      ],
      totalUnits: 32
    },
    {
     name:'Special',
     desc: "Special power!",
      behaviors: [mod.PIECEBEHAVES.KILLFIRST],
      power: mod.PIECEMAXPOWER + 1,
      move: true,
      totalUnits: 1
    }
];

// Export main object 
module.exports = mod;
