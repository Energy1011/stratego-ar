'use strict'; 

var piecesMod = require('./pieces')

// Declare main export obj
var mod = {};

// States and Behaviors
mod.GAMESTATES = {'WAITTURN':-4,'WAITRESPONSE':-3,'RESET':-2,'CONTINUE':-1,'OK':0,'ERROR':1, 'SETUP':2, 'READY':3, 'PLAYING':4, 'FINISHED':5};
mod.TURNTYPE = {'SIMPLE':0, 'ATTACK':1};
mod.TILETYPE = {'EMPTY':-1, 'ENEMY':-2};
mod.BATTLERESULT = {'WIN':1, 'LOSE':2, 'DRAW':3};
// Current game data
mod.gameData = {};

mod.tile = {
  piece: null
}

mod.createNewGame = function(){
  var gID = createHash();
  var plID = createHash();
  var roomID = createHash();
  console.log('Created gID: '+gID);
  var created = new Date().toString();
  // TODO Create it in db
  mod.gameData[gID]= {
    gID: gID,
    roomID: roomID,
    playersID: [], // players names
    players: [], // players names
    tables: [], // players tables 
    table:[], // server table
    //pieceTypes: piecesMod.pieceTypes,
    pieceCounters: piecesMod.pieceCounters,
    status: mod.GAMESTATES.SETUP,
    lastTurn: null,
    turnCounter: 0,
    created: created,
    createdBy: plID
  };
  // random turn , two players
  mod.gameData[gID].lastTurn = Math.floor(Math.random() * 2);
  // filter data
  return {
    gID: gID, 
    plID: plID, 
    pieceCounters: mod.gameData[gID].pieceCounters
  };
}

mod.joinGame = function(d){
  var plID = createHash();
  if(typeof mod.gameData[d.gID] == 'undefined') return {error:'Not existing game'};
  // TODO check for game status == INIT
  return {
    gID: d.gID,
    plID: plID,
    pieceCounters: piecesMod.pieceCounters
  };
}

mod.createServerTable= function(gID){
  var tile = 0;
  for(var e in mod.gameData[gID].tables[0]){
    mod.gameData[gID].table.push({ plIndex:0, piece:mod.gameData[gID].tables[0][e], tile:tile });
    tile++;
  }
  for(var e=0; e<20; e++){
    mod.gameData[gID].table.push({ plIndex:mod.TILETYPE.EMPTY, piece:mod.TILETYPE.EMPTY, tile:tile});
    tile++;
  }
  for(var e in mod.gameData[gID].tables[0]){
    mod.gameData[gID].table.push({ plIndex:1, piece:mod.gameData[gID].tables[1][(e - 39) * -1], tile:tile });
    tile++;
  }
  return true;
}

mod.addPlayerReady = function(d){
  // TODO: Check for current game status
  var res = {gID: d.gID, plID:d.plID, status: mod.GAMESTATES.SETUP};
  console.log('Adding player ready to game..');
  if(mod.gameData[d.gID].players.length >= 2){
    res.error = 'There is not space for other player.';
    return res;
  }
  console.log('Adding player name...');
  mod.gameData[d.gID].players.push(d.plName);
  console.log('Adding player table...');
  mod.gameData[d.gID].tables.push(d.plT);
  console.log('Adding player id...');
  mod.gameData[d.gID].playersID.push(d.plID);
  console.log(mod.gameData);
  if(mod.gameData[d.gID].players.length == 2){
    // combine and create server table
    mod.createServerTable(d.gID); 
    res.status= mod.GAMESTATES.READY;
  }else{
    mod.gameData[d.gID].status = mod.GAMESTATES.SETUP;
  } 
  return res;
}

mod.checkGameExists = function(gID){
  return (typeof mod.gameData[gID] != 'undefined');
}

mod.checkOpponentsReady = function(d){
  // TODO: Check for current game status
  if(mod.checkGameExists(d.gID) &&
    typeof mod.gameData[d.gID].players != 'undefined' &&
    mod.gameData[d.gID].players.length == 2){
      mod.gameData[d.gID].status = mod.GAMESTATES.READY;
      return {status: mod.GAMESTATES.READY, gID:d.gID};
  }else{
    return {msg:'Waiting for a player', status: mod.GAMESTATES.SETUP};
  }
}

mod.getPlIndex = function(gID, plID){
  return (mod.gameData[gID].playersID[0]== plID)? 0:1;
}

mod.getGameDataByPlID = function(gID, plID){
  var plIndex = mod.getPlIndex(gID, plID);
  return { 
    gID: gID,
    plID: plID,
    players: mod.gameData[gID].players,
    plTable: mod.gameData[gID].tables[plIndex],
    lastTurn: mod.gameData[gID].lastTurn,
    plIndex: mod.getPlIndex(gID,plID)
  } 
}

// function to return main gameData and join socket to the game room
mod.startGame = function (d){
  if(typeof mod.gameData[d.gID] == 'undefined') return {error:'Not existing game'};
  return mod.getGameDataByPlID(d.gID, d.plID);
}

function checkGameStatus(gID, s){
 return (mod.gameData[gID].status == s) ? true : false;
}

function createHash(){
  var crypto = require('crypto');
  var current_date = (new Date()).valueOf().toString();
  var random = Math.random().toString();
  return crypto.createHash('sha1').update(current_date + random).digest('hex');
}

// GAME LOGIC
mod.playerJoinRoom = function(d, socket){
  console.log('Joining player to room');
  var res = { status:mod.GAMESTATES.ERROR };

  if(!mod.checkGameExists(d.gID)){
    res.error = "Game doesn't exist";
    socket.emit('server:game-player-join-game', res);
    return mod.GAMESTATES.ERROR;
  }

  var roomID = mod.gameData[d.gID].roomID;
  console.log('roomID :'+roomID);
  socket.join(roomID,()=>{
    res.status = mod.GAMESTATES.OK;
    socket.emit('server:game-player-join-game', res);
    return mod.GAMESTATES.OK;
  });
}

mod.resolveBattle = function(d, origin, target){
// FLAG
  if(target.piece == piecesMod.PIECETYPES.FLAG){
    console.log('Player win the game!!');
    d.bResult = mod.BATTLERESULT.WIN;
    d.winGame = mod.BATTLERESULT.WIN;
    return d;
  }
// SPECIAL
  if(origin.piece == piecesMod.PIECETYPES.SPECIAL){
    console.log('Win the attack with special');
    d.bResult = mod.BATTLERESULT.WIN;
    return d;
  }
  if(target.piece == piecesMod.PIECETYPES.SPECIAL){
    console.log('Special killed.');
    d.bResult = mod.BATTLERESULT.WIN;
    return d;
  }
// BOMB
  if(target.piece == piecesMod.PIECETYPES.BOMB){
    // Do we have a bomb deactivator ?
    if( origin.piece != piecesMod.PIECETYPES.BOMBDEACTIVATOR){
      d.bResult = mod.BATTLERESULT.DRAW;
      console.log('BOMB!');
      return d;
    }else{
      console.log('BOMB Deactivated!');
      d.bResult= mod.BATTLERESULT.WIN;
      return d;
    }
  }
//SOLDIERS
  if(origin.piece == target.piece){
    console.log('Draw !!');
    d.bResult = mod.BATTLERESULT.DRAW;
    return d;
  }
  if(parseInt(origin.piece) > parseInt(target.piece)){
    console.log('Win by range!!');
    d.bResult = mod.BATTLERESULT.WIN;
  }else{
    console.log('Lose by range!!');
    d.bResult = mod.BATTLERESULT.LOSE;
  }
  return d;
}

mod.serverTableUpdateBattle = function(d, oIndex, tIndex){
  // Target tile is removed from server table game too
  if(d.bResult == mod.BATTLERESULT.DRAW){
    mod.gameData[d.gID].table[tIndex].plIndex = mod.gameData[d.gID].table[tIndex].piece = mod.TILETYPE.EMPTY;
  }

  // Target piece is removed, and origin is set to target
  if(d.bResult == mod.BATTLERESULT.WIN){
    mod.gameData[d.gID].table[tIndex].plIndex = mod.gameData[d.gID].table[oIndex].plIndex;
    mod.gameData[d.gID].table[tIndex].piece = mod.gameData[d.gID].table[oIndex].piece;
  }

  // all cases (draw, lose, win) origin tile is set to empty
  mod.gameData[d.gID].table[oIndex].plIndex =  mod.TILETYPE.EMPTY;
  mod.gameData[d.gID].table[oIndex].piece =  mod.TILETYPE.EMPTY;
}

mod.validateMove = function(d){
  var res = {status: mod.GAMESTATES.ERROR};
  // TODO: validate mov distance
  // flip table for server table ?
  if(d.plIndex){
    // origin index and target index flipped 
    var oIndex = (d.move[0] - 99) * - 1;
    var tIndex = (d.move[1] - 99) * - 1;
  }else{
    var oIndex = d.move[0];
    var tIndex = d.move[1];
  }
    var origin = mod.gameData[d.gID].table[oIndex];
    var target = mod.gameData[d.gID].table[tIndex];

  // validate you own origin piece
  if(origin.plIndex != d.plIndex){
    res.error = 'You can only move your pieces';
    return res;
  }
  // validate bomb of flag cant move
  if(origin.piece == piecesMod.PIECETYPES.BOMB ||
    origin.piece == piecesMod.PIECETYPES.FLAG){
    res.error = 'You can not move flag or bombs by rules';
    return res;
  } 
  // validate target piece
  if(target.plIndex == d.plIndex){
    // cant move over own piece 
    res.error = 'You can not move on your own pieces';
    return res;
  }

  // determinate kind of move: simple or attack
  if(target.plIndex == mod.TILETYPE.EMPTY){
    console.log('Type of move detected is simple.');
    res.turnType = mod.TURNTYPE.SIMPLE;
    // Update server table
      mod.gameData[d.gID].table[tIndex].plIndex = mod.gameData[d.gID].table[oIndex].plIndex;
      mod.gameData[d.gID].table[tIndex].piece = mod.gameData[d.gID].table[oIndex].piece;
      mod.gameData[d.gID].table[oIndex].plIndex = mod.TILETYPE.EMPTY;
      mod.gameData[d.gID].table[oIndex].piece = mod.TILETYPE.EMPTY;
  }else{
    if(target.plIndex == (1 - d.plIndex)){
      console.log('Type of move detected attack !');
      res.turnType = mod.TURNTYPE.ATTACK;
      res.pieceOVal = origin.piece;
      res.pieceTVal = target.piece;
      d = mod.resolveBattle(d, origin, target);
    // Update server table
      mod.serverTableUpdateBattle(d, oIndex, tIndex);
      res.bResult = d.bResult; 
      if(typeof d.winGame != 'undefined') res.winGame = d.winGame;
    }
  }

  // Response move data: needed for opponents client
  res.move = d.move;

  // Toggle player turn
  mod.gameData[d.gID].lastTurn = res.lastTurn = 1 - mod.gameData[d.gID].lastTurn;

  res.status = mod.GAMESTATES.OK;
  return res;
}

mod.playerMove = function(d, socket){
  console.log('Validating the move..');
  d.plIndex = mod.getPlIndex(d.gID, d.plID);
  // Validate move
  var res = mod.validateMove(d);
  // if move is valid
  if(typeof res.error == 'undefined'){
    socket.emit('server:game-player-move', res);
    // always flip table move response to opponents client
    res.move[0] = (d.move[0] - 99) * -1;
    res.move[1] = (d.move[1] - 99) * -1;
    // notify the move to the opponent
    var roomID = mod.gameData[d.gID].roomID;
    socket.to(roomID).emit('server:game-opponent-turn', res);
  }else{
    console.log('Error validating the move: ', res.error);
  }
}

// Export main object 
module.exports = mod;
