const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

var cookieParser = require('cookie-parser');
var session = require('express-session')

app.use(cookieParser());
app.use(session({
    secret: 'devdevdev', // just a long random string
    resave: false,
    saveUninitialized: true
}));

app.set('view engine', 'pug');

// Libs, Modules
var GameMod= require('./lib/game')

// middleware static
app.use(express.static('public'));

// Setup route
app.get('/setup/:gID',(req, res)=>{
  //console.log('express id:'+req.sessionID);
  //console.log('game id:'+req.params.gID);
  res.render('setup',{gID:req.params.gID});
  //res.status(200).send("It works from node...");
});

// Main game route 
app.get('/game/:gID/:plID',(req, res)=>{
  console.log('Route game and starting game..');
  res.render('game',{gD:JSON.stringify(GameMod.startGame(req.params))});
});

// default route
app.get('*',function (req, res) {
  res.redirect('/setup/0');
});

// Events
io.on('connection',(socket)=>{
  console.log('Connected from a client/socket');

  // Player create a game
  socket.on('client:create-game',(data)=>{
    console.log('\nclient:create-game');
    //EMIT Join existing game
    socket.emit('server:create-game', GameMod.createNewGame(data));
  });

  // Player join game
  socket.on('client:join-game',(data)=>{
    var gameData = GameMod.joinGame(data);
    //EMIT Join existing game
    socket.emit('server:join-game', GameMod.joinGame(data));
  });

  // Player ready to game
  socket.on('client:ready-game',(data)=>{
    console.log('\nPlayer Ready for the game:'+data.plName);
    //EMIT
    socket.emit('server:ready-game',GameMod.addPlayerReady(data));
  });

  // Player asking for game ready, oppponent
  socket.on('client:check-opponent-ready-game',(data)=>{
    console.log('\nclient:check-opponent-ready-game:');
    console.log('socket id',socket.id)
    //EMIT
    socket.emit('server:check-opponent-ready-game', GameMod.checkOpponentsReady(data));
  });

// GAME LOGIC
  // Player move
  socket.on('client:game-player-move',(data)=>{
    console.log('\nclient:game-player-move:');
    GameMod.playerMove(data,socket);
  });

  socket.on('client:game-player-join-room',(data)=>{
    console.log('\nclient:game-player-join-room');
    GameMod.playerJoinRoom(data, socket);
  });
});//End connection

server.listen(8080,()=>{
  console.log("Server running");
});
